<?php

use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase
{


    public function test1()
    {
        $filePath = __DIR__ . '/acl.cfg';
        $parser   = new \Ox3a\AclParser();

        $this->assertEquals($parser->parse($filePath), [
            [
                'branch' => '',
                'key'    => 'branch1',
                'type'   => 0,
                'title'  => 'Branch 1',
            ],
            [
                'branch' => 'branch1',
                'key'    => 'branch11',
                'type'   => 0,
                'title'  => 'Branch 11',
            ],
            [
                'branch' => 'branch1',
                'key'    => 'branch12',
                'type'   => 0,
                'title'  => 'Branch 12',
            ],
            [
                'branch' => 'branch1>branch12>a',
                'key'    => 'access',
                'type'   => 1,
                'title'  => 'Access 11 12',
            ],
            [
                'branch' => 'branch1>branch11>a',
                'key'    => 'access',
                'type'   => 1,
                'title'  => 'Access 11 12',
            ],
            [
                'branch' => '',
                'key'    => 'branch2',
                'type'   => 0,
                'title'  => 'Branch 2',
            ],
            [
                'branch'  => 'branch2',
                'key'  => 'access',
                'type'  => 1,
                'title' => 'Access 2',
            ],
        ]);
    }


}
