<?php


namespace Ox3a;


class AclParser
{

    const COMMENT_REGEXP = '/^#/';


    public function parse($filePath)
    {
        if (!is_file($filePath)) {
            throw new \RuntimeException('File not found');
        }

        $result = [];

        $file = fopen($filePath, 'r');

        while (false !== ($row = fgets($file))) {
            $row = trim($row);
            if (!$row || preg_match(self::COMMENT_REGEXP, $row)) {
                continue;
            }

            $rows = $this->prepareRow($row);
            foreach ($rows as $row) {
                $result[] = $this->parseRow($row);
            }

        }

        fclose($file);

        return $result;
    }


    public function parseRow($row)
    {
        $data = explode(' ', $row, 3);

        $path   = explode('>', $data[0]);
        $key    = current(array_splice($path, -1, 1));
        $branch = implode('>', $path);

        return [
            'branch' => $branch,
            'key'    => $key,
            'type'   => (int)$data[1],
            'title'  => $data[2],
        ];
    }


    public function prepareRow($row)
    {
        $subCommandsRegEx = '/\[[(\w+),?]+\]/u';
        $subElementRegEx  = '/(\w+)/';

        if (preg_match_all($subCommandsRegEx, $row, $matches)) {
            $commands = [];

            if (preg_match_all($subElementRegEx, $matches[0][0], $subCommands)) {
                foreach ($subCommands[0] as $num => $subRoute) {
                    $subCommand = str_replace($matches[0][0], $subRoute, $row);

                    $subCommands = $this->prepareRow($subCommand);
                    if (is_array($subCommands)) {
                        $commands = array_merge($commands, $subCommands);
                    } else {
                        $commands[] = $subCommands;
                    }
                }
            }

            return $commands;
        } else {
            return [$row];
        }
    }

}
